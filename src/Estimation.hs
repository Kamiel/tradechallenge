{-# LANGUAGE OverloadedStrings #-}

{-|
Module      : Estimation
Description : Maximum profit strategy creator for daily share prices
Copyright   : (c) Pitomets Yuriy, 2014
License     : PublicDomain
Maintainer  : pitometsu@gmail.com
Stability   : experimental
Portability : POSIX

== Algorithm description.
Process prices for each day step by step.
On each step try to find day with min price.
If there more then one --- take right.
After try to find day with max price in right of min day.
If there more then one --- take left.
In case if we found both, mark for buy cheaper
and for sell --- expensive, otherwise mark day as none.
Iterate the same way remaining unmarked days.

== Algorithmic complexity
According to description, algorithmic complexity is __O(N^2)__
because with given N data each element will processed N times.
-}
module Estimation
    (
      -- * Types
      TestCase

      -- * Functions
    , solve
    ) where

import Data.Maybe (isJust)
import Data.List (mapAccumL)

import Data.ByteString.Lazy (ByteString, append)
import qualified Text.Show.ByteString as B

import Data.List.Zipper
    ( Zipper(..)
    , toList
    , fromList
    , fromListEnd
    , replace
    , cursor
    , right
    , endp
    )

-- | trading 'Action' for each day
data Action  = Buy   -- ^ 'Buy' share
             | Sell  -- ^ 'Sell' share
             | None  -- ^ 'None' at this 'Day'
             deriving Show

instance B.Show Action where
  showp Buy  = B.putAsciiStr "Buy"
  showp Sell = B.putAsciiStr "Sell"
  showp None = B.putAsciiStr "None"

type Price    = Int      -- ^ 'Price' (of share)
type TestCase = [Price]  -- ^ 'TestCase' --- 'Price's list for each 'Day'

-- | 'Day' with 'Price' for share probably marked for 'Action'
data Day = Day { action :: !(Maybe Action)  -- ^ trading 'action'
               , price  :: !Price           -- ^ share 'price'
               } deriving Show

instance Eq Day where
  x == y = price x == price y

instance Ord Day where
  x `compare` y = price x `compare` price y

-- | 'Condition' is type for comparison 'Day's
type Condition = Day -> Day -> Bool

-- | 'format' marked 'Day's according to output format and sum their 'Price's
format :: [Day] -> ByteString
format e = B.show profit `append` " " `append` B.show actions
  where (profit, actions)   =  mapAccumL profitActions 0 e
        profitActions ps e' = (ps + price e', toAction $ action e')
        toAction (Just a)   = a
        toAction Nothing    = None

-- | 'strategy' for solving test cases with maximum profit
strategy :: [Day] -> [Day]
strategy e | endp minDay = toList minDay
           | otherwise   = strategy . toList $! uncurry mark logic
  where
    minDay = best (<=) $ fromList e
    maxDay = best (>) . right $ mark Buy minDay
    skip   = (None, minDay)
    deal   = (Sell, maxDay)
    logic  | endp maxDay                   = skip
           | cursor maxDay > cursor minDay = deal
           | otherwise                     = skip

-- | 'best' 'Day' by 'Condition'
best :: Condition -> Zipper Day -> Zipper Day
best c z = find c z z

-- | 'find' greatest day for 'best'
find :: Condition   -- ^ condition
     -> Zipper Day  -- ^ greatest day
     -> Zipper Day  -- ^ iterated day
     -> Zipper Day
find c z z' | not $ endp z'              = find c g $ right z'
            | endp z                     = z
            | isJust . action $ cursor z = fromListEnd $ toList z
            | otherwise                  = z
  where
    e  = cursor z
    e' = cursor z'
    g  | isJust $ action e' = z
       | isJust $ action e  = z'
       | e' `c` e           = z'
       | otherwise          = z

-- | 'mark' current 'Day' by 'Action'
mark :: Action -> Zipper Day -> Zipper Day
mark a z | endp z    = z
         | otherwise = replace e z
  where
    e = Day { action = Just a
            , price  = price $ cursor z }

-- | 'solve' 'TestCase' and return solution in proper format
solve :: TestCase -> ByteString
solve = format . strategy . days
  where
    days = map $ \p -> Day { action = Nothing
                           , price  = p }
