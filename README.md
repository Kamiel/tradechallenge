## Maximum profit strategy creator for daily share prices

#### Algorithm description.
Process prices for each day step by step.  
On each step try to find day with min price.  
If there more then one --- take right.  
After try to find day with max price in right of min day.  
If there more then one --- take left.  
In case if we found both, mark for buy cheaper  
and for sell --- expensive, otherwise mark day as none.  
Iterate the same way remaining unmarked days.  

#### Algorithmic complexity
According to description, algorithmic complexity is __O(N^2)__
because with given N data each element will processed N times.

###Installation
    $ cabal sandbox init
    $ cabal install --only-dependencies
    $ cabal build

###Usage
    $ echo "5 4 3 6 8
    1 3 2 4
    1 4 2 3
    6 4 3
    1 2 3
    " | ./dist/build/TradeChallenge/TradeChallenge
    26 [None,Buy,Buy,Sell,Sell]
    10 [Buy,None,None,Sell]
    10 [Buy,Sell,Buy,Sell]
    13 [None,None,None]
    6 [Buy,None,Sell]
